%{
int realnum_count = 0;
%}
digit [0-9]
%%
{digit}*(\.{digit}+)? {++realnum_count;
printf("[realnum: %s - %f]",yytext,atof(yytext));}
%%
int main(){
    yylex();
    printf("The number of real numbers: %d",realnum_count);
    return 0;
}
